﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BookListImporter.Lib.Implementation;
using BookListImporter.Lib.Model;
using FluentAssertions;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace BookListImporter.Lib.Test
{
    [TestFixture]
    public class BookItemParserTests
    {
        private BookItemParser _parser;

        [SetUp]
        public void Setup()
        {
            var typeADef = new TypeADefinition();
            var typeBDef = new TypeBDefinition();

            _parser = new BookItemParser();
            _parser.AddDefinition(ImporterType.A, typeADef);
            _parser.AddDefinition(ImporterType.B, typeBDef);
        }

        [Test]
        public void Parse_SupportedTypeA_ReturnCorrectBookItem()
        {
            var testInput = "Charlie Bone Series 1234567890           Jenny Nimmo";
            var bookItem = _parser.Parse(ImporterType.A, testInput);

            bookItem.Name.Should().Be("Charlie Bone Series");
            bookItem.ISBN.Should().Be("1234567890");
            bookItem.Author.Should().Be("Jenny Nimmo");
        }

        [Test]
        public void Parse_SupportedTypeB_ReturnCorrectBookItem()
        {
            var testInput = "A Fine and Private Place      4334445345564        Peter S. Beagle";
            var bookItem = _parser.Parse(ImporterType.B, testInput);
            bookItem.Name.Should().Be("A Fine and Private Place");
            bookItem.ISBN.Should().Be("4334445345564");
            bookItem.Author.Should().Be("Peter S. Beagle");
        }

        [Test]
        public void Parse_SupportedTypeUnsupported_ThrowNotsupportedException()
        {
            var testInput = "random string";
            ActualValueDelegate<object> testDelegate = () => _parser.Parse(ImporterType.NotSupported, testInput);
            Assert.That(testDelegate, Throws.TypeOf<NotSupportedException>(), "Import Type " + ImporterType.NotSupported + " not supported!");
        }
    }
}
