﻿using System.IO;
using BookListImporter.Lib.Implementation;
using BookListImporter.Lib.Interface;
using BookListImporter.Lib.Model;
using FluentAssertions;
using NUnit.Framework;

namespace BookListImporter.Lib.Test
{
    [TestFixture]
    public class RecordReaderTests
    {
        private IRecordReader _reader;

        [SetUp]
        public void Setup()
        {
            var typeADef = new TypeADefinition();
            var typeBDef = new TypeBDefinition();
            _reader = new RecordReader();

            _reader.AddDefinition(ImporterType.A, typeADef);
            _reader.AddDefinition(ImporterType.B, typeBDef);
        }

        [Test]
        public void ReadLine_WhiteSpaceWithTabs_ReturnCorrectBookItem()
        {
            var testInput = "A Fine and Private Place 	  4334445345564		   Peter S. Beagle";
            var typeADef = new TypeADefinition();
            var typeBDef = new TypeBDefinition();
            var parser = new BookItemParser();
            parser.AddDefinition(ImporterType.A, typeADef);
            parser.AddDefinition(ImporterType.B, typeBDef);

            using (var ms = new MemoryStream())
            {
                StreamWriter sw = new StreamWriter(ms);
                sw.WriteLine(testInput);
                sw.Flush();
                ms.Position = 0;
                StreamReader sr = new StreamReader(ms);

                var input = _reader.ReadLine(ImporterType.B, sr);
                var bookItem = parser.Parse(ImporterType.B, input);
                bookItem.Name.Should().Be("A Fine and Private Place");
                bookItem.ISBN.Should().Be("4334445345564");
                bookItem.Author.Should().Be("Peter S. Beagle");
            }
        }

        [Test]
        public void ReadLine_TypeANoWhiteSpace_ReturnCorrectBookItem()
        {
            var testInput = "Among Othersabcdefgh345343433440123456789Jo Walton";
            var typeADef = new TypeADefinition();
            var typeBDef = new TypeBDefinition();
            var parser = new BookItemParser();
            parser.AddDefinition(ImporterType.A, typeADef);
            parser.AddDefinition(ImporterType.B, typeBDef);

            using (var ms = new MemoryStream())
            {
                StreamWriter sw = new StreamWriter(ms);
                sw.WriteLine(testInput);
                sw.Flush();
                ms.Position = 0;
                StreamReader sr = new StreamReader(ms);

                var input = _reader.ReadLine(ImporterType.A, sr);
                var bookItem = parser.Parse(ImporterType.A, input);
                bookItem.Name.Should().Be("Among Othersabcdefgh");
                bookItem.ISBN.Should().Be("345343433440123456789");
                bookItem.Author.Should().Be("Jo Walton");
            }
        }

        [Test]
        public void ReadLine_TypeBNoWhiteSpace_ReturnCorrectBookItem()
        {
            var testInput = "Letters from a Lost Uncleaaaaa453453535353512345678Mervyn Peake";
            var typeADef = new TypeADefinition();
            var typeBDef = new TypeBDefinition();
            var parser = new BookItemParser();
            parser.AddDefinition(ImporterType.A, typeADef);
            parser.AddDefinition(ImporterType.B, typeBDef);

            using (var ms = new MemoryStream())
            {
                StreamWriter sw = new StreamWriter(ms);
                sw.WriteLine(testInput);
                sw.Flush();
                ms.Position = 0;
                StreamReader sr = new StreamReader(ms);

                var input = _reader.ReadLine(ImporterType.B, sr);
                var bookItem = parser.Parse(ImporterType.B, input);
                bookItem.Name.Should().Be("Letters from a Lost Uncleaaaaa");
                bookItem.ISBN.Should().Be("453453535353512345678");
                bookItem.Author.Should().Be("Mervyn Peake");
            }
        }
    }
}
