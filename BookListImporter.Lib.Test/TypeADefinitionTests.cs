﻿using System;
using BookListImporter.Lib.Implementation;
using FluentAssertions;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace BookListImporter.Lib.Test
{
    [TestFixture]
    public class TypeADefinitionTests
    {
        private TypeADefinition _importer;

        [SetUp]
        public void Setup()
        {
            _importer = new TypeADefinition();
        }

        [Test]
        public void ParseBookItem_StringShorterThanSpecified_ThrowFormatExceptionIncorrectFormatMessage()
        {
            var testInput = "";
            ActualValueDelegate<object> testDelegate = () => _importer.ParseBookItem(testInput);
            Assert.That(testDelegate, Throws.TypeOf<FormatException>(), "Input string is longer than expected!");
        }

        [Test]
        public void ParseBookItem_StringLongerThanSpecified_ThrowFormatExceptionWithIncorrectFormatMessage()
        {
            // Test with 90 chars.
            var testInput = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
            ActualValueDelegate<object> testDelegate = () => _importer.ParseBookItem(testInput);
            Assert.That(testDelegate, Throws.TypeOf<FormatException>(), "Input string is shorter than expected!");
        }

        [Test]
        public void ParseBookItem_StringFormatCorrect_ReturnCorrectBookItem()
        {
            var testInput = "Charlie Bone Series 1234567890           Jenny Nimmo";
            var bookItem = _importer.ParseBookItem(testInput);
            bookItem.Name.Should().Be("Charlie Bone Series");
            bookItem.ISBN.Should().Be("1234567890");
            bookItem.Author.Should().Be("Jenny Nimmo");
        }
    }
}
