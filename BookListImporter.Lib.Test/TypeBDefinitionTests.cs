﻿using System;
using BookListImporter.Lib.Implementation;
using FluentAssertions;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace BookListImporter.Lib.Test
{
    public class TypeBDefinitionTests
    {
        private TypeBDefinition _importer;

        [SetUp]
        public void Setup()
        {
            _importer = new TypeBDefinition();
        }

        [Test]
        public void ParseBookItem_StringShorterThanSpecified_ThrowFormatExceptionIncorrectFormatMessage()
        {
            var testInput = "";
            ActualValueDelegate<object> testDelegate = () => _importer.ParseBookItem(testInput);
            Assert.That(testDelegate, Throws.TypeOf<FormatException>(), "Input string is longer than expected!");
        }

        [Test]
        public void ParseBookItem_StringLongerThanSpecified_ThrowFormatExceptionWithIncorrectFormatMessage()
        {
            // Test with 90 chars.
            var testInput = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
            ActualValueDelegate<object> testDelegate = () => _importer.ParseBookItem(testInput);
            Assert.That(testDelegate, Throws.TypeOf<FormatException>(), "Input string is shorter than expected!");
        }

        [Test]
        public void ParseBookItem_StringFormatCorrect_ReturnCorrectBookItem()
        {
            var testInput = "A Fine and Private Place      4334445345564        Peter S. Beagle";
            var bookItem = _importer.ParseBookItem(testInput);
            bookItem.Name.Should().Be("A Fine and Private Place");
            bookItem.ISBN.Should().Be("4334445345564");
            bookItem.Author.Should().Be("Peter S. Beagle");
        }
    }
}
