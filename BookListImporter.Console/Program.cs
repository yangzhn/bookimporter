﻿using System;
using System.IO;
using BookListImporter.Lib.Implementation;
using BookListImporter.Lib.Model;

namespace BookListImporter
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This program reads input from std input and prints results to std output.\n");
            var inputStream = Console.OpenStandardInput();
            //var inputStream = File.Open("A.txt", FileMode.Open);
            var sr = new StreamReader(inputStream);
            
            var typeADef = new TypeADefinition();
            var typeBDef = new TypeBDefinition();

            var bookItemParser = new BookItemParser();
            bookItemParser.AddDefinition(ImporterType.A, typeADef);
            bookItemParser.AddDefinition(ImporterType.B, typeBDef);

            var recordReader = new RecordReader();
            recordReader.AddDefinition(ImporterType.A, typeADef);
            recordReader.AddDefinition(ImporterType.B, typeBDef);

            var currentLine = sr.ReadLine();
            ImporterType type;

            if (!Enum.TryParse(currentLine.Trim(), true, out type))
            {
                Console.WriteLine("File type not recogonized!");
                return;
            }

            try
            {
                while ((currentLine = recordReader.ReadLine(type, sr)) != string.Empty)
                {
                    var bookItem = bookItemParser.Parse(type, currentLine);
                    Console.WriteLine(bookItem.Name + "," + bookItem.ISBN + "," + bookItem.Author);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
