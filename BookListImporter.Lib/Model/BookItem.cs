﻿namespace BookListImporter.Lib.Model
{
    public class BookItem
    {
        public string Name { get; set; }
        public string ISBN { get; set; }
        public string Author { get; set; }
    }
}
