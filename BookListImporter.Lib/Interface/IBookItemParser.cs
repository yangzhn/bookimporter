﻿using BookListImporter.Lib.Model;

namespace BookListImporter.Lib.Interface
{
    public interface IBookItemParser
    {
        /// <summary>
        /// Parse string of specified type as BookItem.
        /// </summary>
        /// <param name="t">ImporterType</param>
        /// <param name="input">string</param>
        /// <returns>BookItem</returns>
        BookItem Parse(ImporterType t, string input);

        /// <summary>
        /// Add supported BookItemDefinition to this record reader.
        /// </summary>
        /// <param name="t">ImporterType</param>
        /// <param name="definition">IBookItemDefinition</param>
        void AddDefinition(ImporterType t, IBookItemDefinition definition);
    }
}
