﻿using System.IO;
using BookListImporter.Lib.Model;

namespace BookListImporter.Lib.Interface
{
    public interface IRecordReader
    {
        /// <summary>
        /// Read a whole line of input data and covert all tabs to spaces.
        /// </summary>
        /// <param name="t">ImporterType</param>
        /// <param name="sr">StreamReader</param>
        /// <returns>Returns empty string on EOF or .</returns>
        string ReadLine(ImporterType t, StreamReader inputStream);

        /// <summary>
        /// Add supported BookItemDefinition to this record reader.
        /// </summary>
        /// <param name="t">ImporterType</param>
        /// <param name="definition">IBookItemDefinition</param>
        void AddDefinition(ImporterType t, IBookItemDefinition definition);
    }
}
