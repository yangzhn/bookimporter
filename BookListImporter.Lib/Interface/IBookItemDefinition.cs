﻿using BookListImporter.Lib.Model;

namespace BookListImporter.Lib.Interface
{
    public interface IBookItemDefinition
    {
        /// <summary>
        /// Parse input string as BookItem.
        /// </summary>
        /// <param name="input">string</param>
        /// <returns>BookItem</returns>
        BookItem ParseBookItem(string input);

        /// <summary>
        /// Get the starting column index of the next phrase.
        /// </summary>
        /// <param name="currentBreakPoint">Current phrase index</param>
        /// <returns>Next phrase index, 0 based.</returns>
        int GetNextBreakPoint(int currentBreakPoint);
    }
}
