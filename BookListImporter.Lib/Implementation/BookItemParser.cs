﻿using System;
using System.Collections.Generic;
using BookListImporter.Lib.Interface;
using BookListImporter.Lib.Model;

namespace BookListImporter.Lib.Implementation
{
    public class BookItemParser : IBookItemParser
    {
        private readonly Dictionary<ImporterType, IBookItemDefinition> _importers;

        public BookItemParser()
        {
            _importers = new Dictionary<ImporterType, IBookItemDefinition>();
        }
        
        public BookItem Parse(ImporterType t, string input)
        {
            IBookItemDefinition importer;
            if (_importers.TryGetValue(t, out importer))
            {
                return importer.ParseBookItem(input);
            }

            throw new NotSupportedException("Import Type " + t + " not supported!");
        }

        public void AddDefinition(ImporterType t, IBookItemDefinition definition)
        {
            if (_importers.ContainsKey(t))
            {
                _importers.Remove(t);
            }

            _importers.Add(t, definition);
        }
    }
}
