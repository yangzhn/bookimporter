﻿namespace BookListImporter.Lib.Implementation
{
    public class TypeBDefinition : IndexBasedBookItemDefinition
    {
        private const int NameStartIndex = 1;
        private const int NameEndIndex = 30;
        private const int IsbnStartIndex = 31;
        private const int IsbnEndIndex = 51;
        private const int AuthorStartIndex = 52;
        private const int AuthorEndIndex = 72;
        private const int TotalLength = 72;

        public TypeBDefinition()
            : base(
                      NameStartIndex,
                      NameEndIndex,
                      IsbnStartIndex,
                      IsbnEndIndex,
                      AuthorStartIndex,
                      AuthorEndIndex,
                      TotalLength
                  ) { }
    }
}
