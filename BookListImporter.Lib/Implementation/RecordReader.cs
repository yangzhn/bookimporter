﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using BookListImporter.Lib.Interface;
using BookListImporter.Lib.Model;

namespace BookListImporter.Lib.Implementation
{
    public class RecordReader : IRecordReader
    {
        private readonly Dictionary<ImporterType, IBookItemDefinition> _importers;

        public RecordReader()
        {
            _importers = new Dictionary<ImporterType, IBookItemDefinition>();
        }

        public string ReadLine(ImporterType t, StreamReader sr)
        {
            var currentBreakPoint = getBreakPoint(t, 0);
            var sb = new StringBuilder();
            var c = ' ';

            while (c != '\n')
            {
                int val = sr.Read();
                if (val == -1)
                    break;

                c = (char) val;
                if (c != '\t')
                {
                    sb.Append(c);
                }
                else
                {
                    sb.Append(readUntilNonWhiteSpace(sr, sb, currentBreakPoint));
                }

                if (sb.Length - 1 == currentBreakPoint)
                {
                    currentBreakPoint = getBreakPoint(t, currentBreakPoint);
                }
            }

            return sb.ToString().Trim();
        }

        public void AddDefinition(ImporterType t, IBookItemDefinition definition)
        {
            if (_importers.ContainsKey(t))
            {
                _importers.Remove(t);
            }

            _importers.Add(t, definition);
        }

        private int getBreakPoint(ImporterType t, int currentBreakPoint)
        {
            IBookItemDefinition typeDef;
            if (_importers.TryGetValue(t, out typeDef))
            {
                return typeDef.GetNextBreakPoint(currentBreakPoint);
            }

            throw new NotSupportedException("Import Type " + t + " not supported!");
        }

        private char readUntilNonWhiteSpace(StreamReader sr, StringBuilder sb, int breakpoint)
        {
            var toAppend = breakpoint - sb.Length;
            for (int i = 0; i < toAppend; ++i)
            {
                sb.Append(' ');
            }

            var c = (char)sr.Read();
            while (char.IsWhiteSpace(c))
            {
                c = (char)sr.Read();
            }

            return c;
        }
    }
}
