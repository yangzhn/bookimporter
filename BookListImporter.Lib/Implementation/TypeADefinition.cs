﻿namespace BookListImporter.Lib.Implementation
{
    public class TypeADefinition : IndexBasedBookItemDefinition
    {
        private const int NameStartIndex = 1;
        private const int NameEndIndex = 20;
        private const int IsbnStartIndex = 21;
        private const int IsbnEndIndex = 41;
        private const int AuthorStartIndex = 42;
        private const int AuthorEndIndex = 62;
        private const int TotalLength = 62;

        public TypeADefinition()
            : base(
                      NameStartIndex,
                      NameEndIndex,
                      IsbnStartIndex,
                      IsbnEndIndex,
                      AuthorStartIndex,
                      AuthorEndIndex,
                      TotalLength
                  ) {}
    }
}
