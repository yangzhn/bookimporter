﻿using System;
using BookListImporter.Lib.Interface;
using BookListImporter.Lib.Model;

namespace BookListImporter.Lib.Implementation
{
    public abstract class IndexBasedBookItemDefinition : IBookItemDefinition
    {
        private readonly int _nameStartIndex;
        private readonly int _nameEndIndex;
        private readonly int _isbnStartIndex;
        private readonly int _isbnEndIndex;
        private readonly int _authorStartIndex;
        private readonly int _authorEndIndex;
        private readonly int _totalLength;

        protected IndexBasedBookItemDefinition
        (
            int nameStartIndex, 
            int nameEndIndex, 
            int isbnStartIndex, 
            int isbnEndIndex, 
            int authorStartIndex, 
            int authorEndIndex,
            int totalLength
        )
        {
            _nameStartIndex = nameStartIndex - 1;
            _nameEndIndex = nameEndIndex - 1;
            _isbnStartIndex = isbnStartIndex - 1;
            _isbnEndIndex = isbnEndIndex - 1;
            _authorStartIndex = authorStartIndex - 1;
            _authorEndIndex = authorEndIndex - 1;
            _totalLength = totalLength;
        }

        public virtual BookItem ParseBookItem(string input)
        {
            CheckFormat(input);
            
            var result = new BookItem
            {
                Name = input.Substring(_nameStartIndex, _nameEndIndex - _nameStartIndex + 1).Trim(),
                ISBN = input.Substring(_isbnStartIndex, _isbnEndIndex - _isbnStartIndex + 1).Trim(),
                Author = input.Substring(_authorStartIndex).Trim()
            };

            return result;
        }

        public int GetNextBreakPoint(int currentBreakPoint)
        {
            if (currentBreakPoint <= _nameStartIndex)
                return _isbnStartIndex;
            
            return _authorStartIndex;
        }

        protected void CheckFormat(string input)
        {
            if (input.Length > _totalLength)
            {
                throw new FormatException("Input string is longer than expected!");
            }

            if (input.Length < _authorStartIndex + 1)
            {
                throw new FormatException("Input string is shorter than expected!");
            }
        }
        
    }
}
